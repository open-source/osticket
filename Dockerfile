FROM debian:bullseye

# Install System Components
RUN apt update \
    && apt install -y \
    nginx \
    cron \
    lsb-release \
    apt-transport-https \
    curl \
    zip

# Install PHP Components
RUN curl -o /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg \
    && echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | tee /etc/apt/sources.list.d/php.list


RUN apt update && \
    apt install -y \
    libapache2-mod-php8.1 \
    php8.1 \
    php8.1-mysqli \
    php8.1-gd \
    php8.1-gettext \
    php8.1-imap \
    php-json \
    php8.1-mbstring \
    php8.1-xml \
    php8.1-phar \
    php8.1-zip \
    php8.1-intl

WORKDIR /html

RUN sed -i 's/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=1/g' /etc/php/8.1/apache2/php.ini && \
    sed -i 's/expose_php = On/expose_php = Off/g' /etc/php/8.1/apache2/php.ini && \
    sed -i 's/upload_max_filesize = 2M/upload_max_filesize = 30M/g' /etc/php/8.1/apache2/php.ini && \
    sed -i 's/post_max_size = 8M/post_max_size = 30M/g' /etc/php/8.1/apache2/php.ini && \
    ln -s /dev/stderr /var/log/cron.log && \
    (crontab -l ; echo "* * * * * /usr/bin/php8.1 /html/api/cron.php >> /var/log/cron.log 2>&1") | crontab -

WORKDIR /html
EXPOSE 80

COPY ./src /html
COPY config/osticket.conf /etc/apache2/sites-available/osticket.conf

# Enable apache configuration
RUN a2dissite 000-default && \
    rm /etc/apache2/sites-available/000-default.conf && \
    a2enmod rewrite && \
    a2enmod headers && \
    a2ensite osticket && \
    chmod -R g+rX /html && \
    ln -sfT /dev/stderr /var/log/apache2/error.log && \
    ln -sfT /dev/stdout /var/log/apache2/access.log

CMD cron -L /dev/stderr && \
    apache2ctl -DFOREGROUND
